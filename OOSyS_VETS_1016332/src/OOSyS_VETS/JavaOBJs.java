package OOSyS_VETS;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import ljmu.vets.Saveable;
import ljmu.vets.Surgery;


public class JavaOBJs implements Saveable {

	private final String PATH = "M:\\data\\OOSyS\\";
	
	private List<Surgery> surgeries = new ArrayList<Surgery>();

	private Saveable ss;

	//This is the deSerialize method that has been taken directly from the Sys class
	@Override
	public void serialize(List<Surgery> Surgeries) {
		
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(PATH + "surgeries.ser"));
			oos.writeObject(Surgeries);

			// ToDo : Finally ?
			oos.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	@Override
	public List<Surgery> deSerialize() {

		ObjectInputStream ois;

		try {
			ois = new ObjectInputStream(new FileInputStream(PATH + "surgeries.ser"));

			// NOTE : Erasure Warning !
			this.surgeries = (List<Surgery>)ois.readObject();

			// ToDo : Finally ?
			ois.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return surgeries;
		
	}

	

	
}
