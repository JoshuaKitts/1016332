package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import OOSyS_VETS.*;
import ljmu.vets.*;

class JUnitCourse {

	@BeforeEach
	void setUp() throws Exception {
	}

	Surgery p1 = new Surgery("GlynH");
	Surgery s2 = new Surgery("SurgeryA", DayOfWeek.THURSDAY);
	
	@Test
	void test() {
		assertEquals(p1.getPetByName(null), "GlynH");
	}

}
