package ljmu.vets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Cat extends Pet {
	private Breeding breeding;
	
	public Cat(String name, LocalDate regDate, Breeding breeding, Double rate) {
		super(name, regDate, rate);

		this.breeding = breeding;
		this.rate = 13.0;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " +
				this.name + " " +
				this.regDate.format(DateTimeFormatter.ofPattern("dd MMM yy")) + " " +
				this.breeding.toString();
	}

	@Override
	public String toSave(String s) {
		// TODO Auto-generated method stub
		return null;
	}

	// ToDo : get / set Methods ?
}
