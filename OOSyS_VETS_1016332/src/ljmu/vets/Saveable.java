package ljmu.vets;

import java.util.List;

public interface Saveable {

	public List<Surgery> deSerialize();
	
	public void serialize(List<Surgery> Surgeries);
	
}
