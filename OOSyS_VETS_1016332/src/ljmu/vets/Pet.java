package ljmu.vets;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class Pet implements Serializable, Sable {
	protected String name;
	protected LocalDate regDate;
	protected Double rate;

	// ToDo : Private ?
	protected List<Booking> bookings = new ArrayList<Booking>();

	public Pet(String name, LocalDate regDate, Double rate) {
		this.name = name;
		this.regDate = regDate;
		this.rate = rate;
	}

	public void makeBooking(Booking when) {
		this.bookings.add(when);
	}

	public Booking getNextBooking() {
		return this.bookings.stream()
			.filter(o -> o.getWhen().isAfter(LocalDateTime.now()))
				.sorted(Comparator.comparing(o -> o.getWhen()))
					.findFirst().get();
	}

	public String getName() {
		return this.name;
	}

	public LocalDate getRegDate() {
		return this.regDate;
	}
	
	public Double getRate() {
		return this.rate;
	}

	// ToDo : get / set Methods ?
}
