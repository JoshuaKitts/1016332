package ljmu.vets;

import java.util.List;

public interface DataStorage {

	public List<Surgery> get();
	
	public void set(List<Surgery> Surgeries);
	
}
