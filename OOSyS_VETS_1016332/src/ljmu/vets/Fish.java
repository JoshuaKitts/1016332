package ljmu.vets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Fish extends Pet {
	private WaterType waterType;

	public Fish(String name, LocalDate regDate, WaterType waterType, Double rate) {
		super(name, regDate, rate);

		this.waterType = waterType;
		this.rate = 17.0;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " +
				this.name + " " +
				this.regDate.format(DateTimeFormatter.ofPattern("dd MMM yy")) + " " +
				this.waterType.toString();
	}
	
	@Override
	public String toSave(String s) {
		// TODO Auto-generated method stub
		return null;
	}

	// ToDo : get / set Methods ?
}
